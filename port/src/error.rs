use serde_derive::Serialize;
use snafu::Snafu;
use std::sync::Arc;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))] // Required here or else must be declared in using module
pub enum MetricError {
    #[snafu(display("Could not fetch metrics bag: {}", message))]
    MetricsBagUnfetchable { message: String },
    #[snafu(display("Issue serializing metrics: {}", source))]
    SerializeJson {
        #[snafu(source(from(serde_json::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<serde_json::Error>,
    },
    #[snafu(display("Incorrect number of dimensions provided to `with_dims`: {}", msg))]
    IncorrectNumberOfDimensions { msg: String },
    #[snafu(display("{}", str_source))]
    UnderlyingError { str_source: String },
}

impl From<serde_json::Error> for MetricError {
    fn from(e: serde_json::Error) -> Self {
        MetricError::SerializeJson {
            source: Arc::new(e),
        }
    }
}
