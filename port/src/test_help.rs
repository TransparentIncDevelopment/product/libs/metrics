use crate::{
    Counter, Dimensioned, Gauge, Histogram, Metric, MetricDefs, Metrics, MetricsBag,
    MetricsSerializationContainer, Result,
};
#[allow(unused_imports)]
use std::collections::HashMap;
use std::sync::atomic::{AtomicU64, Ordering};
#[allow(unused_imports)]
use std::sync::{Arc, Mutex};

type HistoryDict = Arc<Mutex<HashMap<Vec<String>, DumbHisto>>>;
type InnerDicts = Arc<Mutex<HashMap<Vec<String>, Arc<Mutex<Vec<f64>>>>>>;

#[derive(Clone)]
struct DumbCounter {
    pub val: Arc<AtomicU64>,
}

impl Counter for DumbCounter {
    fn inc(&self) {
        self.val.fetch_add(1, Ordering::SeqCst);
    }

    fn add(&self, delta: u64) {
        self.val.fetch_add(delta, Ordering::SeqCst);
    }

    fn get(&self) -> u64 {
        self.val.load(Ordering::SeqCst)
    }
}
impl Metric for DumbCounter {}

impl Metric for &DumbCounter {}

#[derive(Clone)]
struct DumbGauge {
    pub val: Arc<Mutex<f64>>,
}

impl Gauge for DumbGauge {
    fn inc(&self) {
        *self.val.lock().unwrap() += 1.;
    }

    fn add(&self, delta: f64) {
        *self.val.lock().unwrap() += delta;
    }

    fn get(&self) -> f64 {
        *self.val.lock().unwrap()
    }

    fn set(&self, val: f64) {
        *self.val.lock().unwrap() = val;
    }

    fn dec(&self) {
        *self.val.lock().unwrap() -= 1.;
    }

    fn sub(&self, delta: f64) {
        *self.val.lock().unwrap() -= delta;
    }
}

impl Metric for DumbGauge {}

#[derive(Clone)]
struct DumbHisto {
    history: Arc<Mutex<Vec<f64>>>,
}

impl Histogram for DumbHisto {
    fn observe(&self, v: f64) {
        self.history.lock().unwrap().push(v);
    }
}

impl Metric for DumbHisto {}

#[derive(Default)]
struct DumbDimensionedCounter {
    counters: Arc<Mutex<HashMap<Vec<String>, DumbCounter>>>,
}

impl Dimensioned<dyn Counter> for DumbDimensionedCounter {
    fn with_dims(&self, dim_vals: Vec<&str>) -> Result<Box<dyn Counter>> {
        let owned_dims = dim_vals.iter().map(|x| (*x).to_string()).collect();
        let maybe_existing_ctr = {
            let lock = self.counters.lock().unwrap();
            lock.get(&owned_dims).cloned()
        };
        if let Some(counter) = maybe_existing_ctr {
            Ok(Box::new(counter))
        } else {
            let new_counter = DumbCounter {
                val: Arc::new(AtomicU64::new(0)),
            };
            self.counters
                .lock()
                .unwrap()
                .insert(owned_dims, new_counter.clone());
            Ok(Box::new(new_counter))
        }
    }
}

impl Metric for DumbDimensionedCounter {}

#[derive(Default)]
struct DumbDimensionedGauge {
    gauges: Arc<Mutex<HashMap<Vec<String>, DumbGauge>>>,
}

impl Dimensioned<dyn Gauge> for DumbDimensionedGauge {
    fn with_dims(&self, dim_vals: Vec<&str>) -> Result<Box<dyn Gauge>> {
        let owned_dims = dim_vals.iter().map(|x| (*x).to_string()).collect();
        let maybe_existing_gauge = {
            let lock = self.gauges.lock().unwrap();
            lock.get(&owned_dims).cloned()
        };
        if let Some(gauge) = maybe_existing_gauge {
            Ok(Box::new(gauge))
        } else {
            let new_gauge = DumbGauge {
                val: Arc::new(Mutex::new(0.)),
            };
            self.gauges
                .lock()
                .unwrap()
                .insert(owned_dims, new_gauge.clone());
            Ok(Box::new(new_gauge))
        }
    }
}

impl Metric for DumbDimensionedGauge {}

struct DumbDimensionedHisto {
    histograms: HistoryDict,
    inners: InnerDicts,
}

impl Dimensioned<dyn Histogram> for DumbDimensionedHisto {
    fn with_dims(&self, dim_vals: Vec<&str>) -> Result<Box<dyn Histogram>> {
        let owned_dims: Vec<String> = dim_vals.iter().map(|x| (*x).to_string()).collect();
        let maybe_existing_histo = {
            let lock = self.histograms.lock().unwrap();
            lock.get(&owned_dims).cloned()
        };
        if let Some(histo) = maybe_existing_histo {
            Ok(Box::new(histo))
        } else {
            let new_dict = Arc::new(Mutex::new(Vec::new()));
            let new_histo = DumbHisto {
                history: Arc::clone(&new_dict),
            };
            self.histograms
                .lock()
                .unwrap()
                .insert(owned_dims.clone(), new_histo.clone());
            self.inners.lock().unwrap().insert(owned_dims, new_dict);
            Ok(Box::new(new_histo))
        }
    }
}

impl Metric for DumbDimensionedHisto {}

#[derive(Default)]
pub struct FakeMetrics {
    pub histo_data: Arc<Mutex<Vec<f64>>>,
    pub dim_histo_inners: InnerDicts,
}

impl Metrics for FakeMetrics {
    type ErrorT = ();

    fn initialize<T: MetricsBag>(&self, metrics_bag: &mut T) -> Result<(), ()> {
        for md in metrics_bag.list_metrics() {
            match md {
                MetricDefs::Counter(ctr_def) => ctr_def.populate(Box::new(DumbCounter {
                    val: Arc::new(AtomicU64::new(0)),
                })),
                MetricDefs::Gauge(gauge_def) => gauge_def.populate(Box::new(DumbGauge {
                    val: Arc::new(Mutex::new(0.)),
                })),
                MetricDefs::Histogram(histo_def) => histo_def.populate(Box::new(DumbHisto {
                    history: Arc::clone(&self.histo_data),
                })),
                MetricDefs::DimensionedCtr(dim_ctr_def) => {
                    dim_ctr_def.populate(Box::new(DumbDimensionedCounter::default()))
                }
                MetricDefs::DimensionedGauge(dim_gauge_def) => {
                    dim_gauge_def.populate(Box::new(DumbDimensionedGauge::default()))
                }
                MetricDefs::DimensionedHistogram(dim_histo_def) => {
                    dim_histo_def.populate(Box::new(DumbDimensionedHisto {
                        histograms: Arc::new(Mutex::new(HashMap::new())),
                        inners: Arc::clone(&self.dim_histo_inners),
                    }))
                }
            }
        }
        Ok(())
    }

    fn produce(&self) -> Result<MetricsSerializationContainer, ()> {
        unimplemented!()
    }
}
