#![forbid(unsafe_code)]
#![allow(clippy::float_cmp)]

use metrics_bag_procmacro::MetricsBag;
use xand_metrics::*;

/// Bag of metric definitions adapters can use to make testing easier
#[derive(MetricsBag)]
pub struct TestBag {
    pub test_ctr: CounterDef,
    pub test_gauge: GaugeDef,
    pub test_histo: HistogramDef,
    pub test_dim_ctr: DimensionedCtrDef,
    pub test_dim_gauge: DimensionedGaugeDef,
    pub test_dim_histo: DimensionedHistoDef,
}

impl Default for TestBag {
    fn default() -> Self {
        Self {
            test_ctr: CounterDef::new("test_counter", "counter help!"),
            test_gauge: GaugeDef::new("test_gauge", "gauge help!"),
            test_histo: HistogramDef::new("test_histo", "histogram help!"),
            test_dim_ctr: DimensionedCtrDef::new(
                vec!["a"],
                MetricOpts::new(
                    "test_counter_vec".to_string(),
                    "I have dimensions!".to_string(),
                ),
            ),
            test_dim_gauge: DimensionedGaugeDef::new(
                vec!["a", "b"],
                MetricOpts::new(
                    "test_gauge_vec".to_string(),
                    "I have dimensions!".to_string(),
                ),
            ),
            test_dim_histo: DimensionedHistoDef::new(
                vec!["a", "b", "c"],
                MetricOpts::new(
                    "test_histo_vec".to_string(),
                    "I have dimensions!".to_string(),
                ),
            ),
        }
    }
}

#[test]
fn global_bag_counter() {
    let fake_metrics = FakeMetrics::default();
    let mut tb = TestBag::default();
    fake_metrics.initialize(&mut tb).unwrap();
    // Increment counter
    tb.test_ctr.inc();
    assert_eq!(tb.test_ctr.get(), 1);
    tb.test_ctr.inc();
    assert_eq!(tb.test_ctr.get(), 2);
}

#[test]
fn gauge() {
    let fake_metrics = FakeMetrics::default();
    let mut tb = TestBag::default();
    fake_metrics.initialize(&mut tb).unwrap();
    tb.test_gauge.inc(); // 0 + 1 = 1
    tb.test_gauge.add(10.); // 1 + 10 = 11
    tb.test_gauge.sub(2.); // 11 - 2 = 9
    tb.test_gauge.dec(); // 9 - 1 = 8
    assert_eq!(tb.test_gauge.get(), 8.);
    tb.test_gauge.set(100.);
    assert_eq!(tb.test_gauge.get(), 100.);
}

#[test]
fn histogram() {
    let fake_metrics = FakeMetrics::default();
    let mut tb = TestBag::default();
    fake_metrics.initialize(&mut tb).unwrap();
    // Histogram observation
    tb.test_histo.observe(99.9);
    tb.test_histo.observe(88.8);
    let data = fake_metrics.histo_data.lock().unwrap();
    assert_eq!(data[0], 99.9);
    assert_eq!(data[1], 88.8);
}

#[test]
fn dimension_counter() {
    let fake_metrics = FakeMetrics::default();
    let mut tb = TestBag::default();
    fake_metrics.initialize(&mut tb).unwrap();
    let dims = vec!["testy"];
    tb.test_dim_ctr.with_dims(dims.clone()).unwrap().inc();
    // Retrieve the counter again before checking value
    let val = tb.test_dim_ctr.with_dims(dims).unwrap().get();
    assert_eq!(val, 1);
}

#[test]
fn dimension_gauge() {
    let fake_metrics = FakeMetrics::default();
    let mut tb = TestBag::default();
    fake_metrics.initialize(&mut tb).unwrap();
    let dims = vec!["testy", "messy"];
    let gauge = tb.test_dim_gauge.with_dims(dims.clone()).unwrap();
    gauge.inc();
    gauge.add(10.);
    gauge.sub(2.);
    gauge.dec();
    // Retrieve the gauge again before checking value
    let gauge = tb.test_dim_gauge.with_dims(dims.clone()).unwrap();
    assert_eq!(gauge.get(), 8.);
}

#[test]
fn dimension_histo() {
    let fake_metrics = FakeMetrics::default();
    let mut tb = TestBag::default();
    fake_metrics.initialize(&mut tb).unwrap();
    let dims = vec!["testy", "messy", "zesty"];
    let histo = tb.test_dim_histo.with_dims(dims.clone()).unwrap();
    histo.observe(99.9);
    histo.observe(88.8);
    let owned_dims: Vec<String> = dims.iter().map(|x| (*x).to_string()).collect();
    let dict = fake_metrics.dim_histo_inners.lock().unwrap();
    let data = dict.get(&owned_dims).unwrap().lock().unwrap();
    assert_eq!(data[0], 99.9);
    assert_eq!(data[1], 88.8);
}
