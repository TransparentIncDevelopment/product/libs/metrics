use tpfs_logger_port::logging_event;

#[logging_event]
#[derive(Debug)]
pub(crate) enum LoggingEvent {
    FailedToCreateSocketForHandler(String),
    FailedToSendMetricOverChannel(String),
    FailedToSerializeMetric(String),
    ListenerExitingWithError(String),
    ReceiverThreadExitingWithError(String),
}
