#[logging_event]
pub enum LoggingEvent {
    Msg(String),
    Error(String),
}
