//! This simple app reads metrics (in prometheus' format) on stdin, and then simply exposes them
//! on an HTTP endpoint.
//!
//! This allows our services to spit out metrics on stdout without needing to think about exposing
//! and endpoint themselves.
//!
//! It is meant to be run as a consumer of the stdout of our services, so in the simplest form that
//! could be something like `member-api | prometheus-pipesrv` but in a deployed environment is something
//! like a k8s sidecar
//!
//! It searches for lines that look like this:
//! `{"metrics": [<prometheus metric protobuf binary>]}`

#![forbid(unsafe_code)]

#[macro_use]
extern crate tpfs_logger_port;

mod logging_events;

use hyper::{header::CONTENT_TYPE, rt::Future, service::service_fn_ok, Body, Response, Server};
use lazy_static::lazy_static;
use logging_events::LoggingEvent;
use pipesrv_emitter::SOCKET_PATH;
#[allow(unused_imports)]
use std::path::Path;
use std::time::Duration;
use std::{io, os::unix::net::UnixStream, path::PathBuf, sync::RwLock};
use structopt::StructOpt;
use xand_metrics::MetricsSerializationContainer;

lazy_static! {
    pub static ref LAST_METRICS_CONTENTS: RwLock<String> = RwLock::new("".to_string());
}

const RECONNECT_DURATION: Duration = Duration::from_secs(1);

#[derive(Debug, StructOpt, Clone)]
#[structopt(
    name = "prometheus-pipesrv",
    about = "Serves metrics for scraping by prometheus"
)]
struct Args {
    #[structopt(short, long, default_value = "9898")]
    port: u16,
    #[structopt(
        short,
        long,
        help = "Override default SOCKET_PATH defined in xand-metrics",
        raw(default_value = "SOCKET_PATH")
    )]
    socket_path: String,
}

fn main() -> io::Result<()> {
    let args = Args::from_args();

    tpfs_logger_log4rs_adapter::init_with_default_config().unwrap();

    // Start the http server on another thread
    spawn_server(args.clone());
    incoming_metrics_loop(&args)
}

/// Loops indefinitely, reading in metrics from the specified source and updating
/// [LAST_METRICS_CONTENTS]
fn incoming_metrics_loop(args: &Args) -> io::Result<()> {
    let path: &PathBuf = &Path::new(&args.socket_path).to_path_buf();
    info!(LoggingEvent::Msg(format!(
        "Listening for metrics at {:?}",
        path
    )));
    loop {
        if let Ok(stream) = create_stream(path.clone()) {
            loop {
                match read_stream(&stream) {
                    Ok(read_msg) => {
                        write_last_metric(read_msg);
                    }
                    Err(e) => {
                        debug!(LoggingEvent::Msg(format!(
                            "Couldn't read stream, trying to reconnect: {:?}",
                            e
                        )));
                        break;
                    }
                }
                std::thread::sleep(RECONNECT_DURATION);
            }
        } else {
            debug!(LoggingEvent::Msg(format!("Couldn't connect to {:?}", path)));
            std::thread::sleep(RECONNECT_DURATION);
        }
    }
}

fn create_stream(pbuf: PathBuf) -> std::result::Result<UnixStream, io::Error> {
    UnixStream::connect(pbuf)
}

fn read_stream(
    stream: &UnixStream,
) -> std::result::Result<MetricsSerializationContainer, bincode::Error> {
    let res: MetricsSerializationContainer = bincode::deserialize_from(stream)?;
    Ok(res)
}

/// Writes the metrics inside a [MaybeWrappedLog](enum.MaybeWrappedLog.html) to the static `String` `LAST_METRICS_CONTENTS`
fn write_last_metric(metric: MetricsSerializationContainer) {
    if let Ok(mut contents_ref) = LAST_METRICS_CONTENTS.write() {
        *contents_ref = metric.metrics;
    } else {
        error!(LoggingEvent::Error(
            "ERROR! Couldn't lock last contents.".to_string()
        ))
    }
}

/// Starts the http server on a new thread
fn spawn_server(args: Args) {
    std::thread::spawn(move || {
        let addr = ([0, 0, 0, 0], args.port).into();

        let metric_route = || {
            service_fn_ok(move |_request| {
                if let Ok(body) = LAST_METRICS_CONTENTS.read() {
                    Response::builder()
                        .status(200)
                        .header(CONTENT_TYPE, "text/plain; version=0.0.4")
                        .body(Body::from(body.clone()))
                        .unwrap()
                } else {
                    Response::builder()
                        .status(500)
                        .body(Body::from("Couldn't get lock on last metric contents"))
                        .unwrap()
                }
            })
        };

        let server = Server::bind(&addr).serve(metric_route).map_err(|e| {
            error!(LoggingEvent::Error(format!("Server error: {}", e)));
        });

        hyper::rt::run(server);
    });
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{fs, io::Write, os::unix::net::UnixListener, thread, time::Duration};
    use xand_utils::timeout;

    const IO_TIMEOUT: Duration = Duration::from_secs(2);

    fn start_socket_server_run_callback_and_quit(
        path: &Path,
        callback: (impl Fn(UnixStream) + Send + Sync + 'static),
    ) {
        // Delete old socket if necessary
        if path.exists() {
            fs::remove_file(path).unwrap();
        }

        let socket = UnixListener::bind(path).unwrap();
        thread::spawn(move || loop {
            #[allow(clippy::manual_flatten)]
            for stream in socket.incoming() {
                if let Ok(s) = stream {
                    callback(s);
                    break;
                }
            }
        });
    }

    fn make_send_msg_callback(msg: Vec<u8>) -> (impl Fn(UnixStream) + Send + Sync + 'static) {
        move |mut stream: UnixStream| stream.write_all(&msg).unwrap()
    }

    fn make_send_multiple_msg_callback(msgs: Vec<Vec<u8>>) -> (impl Fn(UnixStream) + Send + Sync) {
        move |mut stream: UnixStream| {
            for msg in msgs.clone() {
                stream.write_all(&msg).unwrap()
            }
        }
    }

    #[test]
    fn can_read_from_socket() {
        let path = Path::new("/tmp/can_read_from_socket");
        let msg = MetricsSerializationContainer {
            metrics: "bleep blop".to_string(),
        };
        let bytes = bincode::serialize(&msg).unwrap();
        let callback = make_send_msg_callback(bytes);
        start_socket_server_run_callback_and_quit(path, callback);
        let stream = create_stream(path.to_path_buf()).unwrap();
        let read_msg = read_stream(&stream).unwrap();
        assert_eq!(msg, read_msg);
    }

    #[test]
    fn empty_socket_reads_none() {
        let path = Path::new("/tmp/empty_socket_reads_none");
        let callback = |_| {};
        start_socket_server_run_callback_and_quit(path, callback);
        let stream = create_stream(path.to_path_buf()).unwrap();
        let read_msg = read_stream(&stream);
        assert!(read_msg.is_err());
    }

    #[test]
    fn can_read_multiple_objects_from_socket() {
        let path = Path::new("/tmp/can_read_multiple_objects_from_socket");
        let msgs = vec![
            MetricsSerializationContainer {
                metrics: "bleep".to_string(),
            },
            MetricsSerializationContainer {
                metrics: "bloop".to_string(),
            },
            MetricsSerializationContainer {
                metrics: "blorp".to_string(),
            },
            MetricsSerializationContainer {
                metrics: "yort".to_string(),
            },
        ];
        let sets_of_bytes: Vec<Vec<u8>> = msgs
            .clone()
            .into_iter()
            .map(|msg| bincode::serialize(&msg).unwrap())
            .collect();
        let callback = make_send_multiple_msg_callback(sets_of_bytes);
        start_socket_server_run_callback_and_quit(path, callback);
        let stream = create_stream(path.to_path_buf()).unwrap();
        for msg in msgs {
            let read_msg = read_stream(&stream).unwrap();
            assert_eq!(msg, read_msg);
        }
    }

    #[test]
    fn full_flow() {
        let path = "/tmp/full_flow";
        let path_buf = Path::new(path).to_path_buf();
        let args = Args {
            port: 34511,
            socket_path: path.to_string(),
        };
        std::thread::spawn(move || incoming_metrics_loop(&args).unwrap());

        // Write a new line to the file. We write this line repeatedly until it works, because
        // the watcher takes some time to get set up, and does not read already-extant lines.
        let sendme = MetricsSerializationContainer {
            metrics: "I'm a metric".to_string(),
        };
        timeout!(IO_TIMEOUT, {
            let bytes = bincode::serialize(&sendme).unwrap();
            let callback = make_send_msg_callback(bytes.clone());
            start_socket_server_run_callback_and_quit(&path_buf, callback);
            "I'm a metric" == *LAST_METRICS_CONTENTS.read().unwrap()
        });
        // Write a line that should be ignored
        let new_line = r#"{"something else": "whatever"}"#;
        timeout!(IO_TIMEOUT, {
            let bytes = bincode::serialize(new_line).unwrap();
            let callback = make_send_msg_callback(bytes.clone());
            start_socket_server_run_callback_and_quit(&path_buf, callback);
            "I'm a metric" == *LAST_METRICS_CONTENTS.read().unwrap()
        });
        // Update it again
        let sendme = MetricsSerializationContainer {
            metrics: "dos".to_string(),
        };
        timeout!(IO_TIMEOUT, {
            let bytes = bincode::serialize(&sendme).unwrap();
            let callback = make_send_msg_callback(bytes.clone());
            start_socket_server_run_callback_and_quit(&path_buf, callback);
            "dos" == *LAST_METRICS_CONTENTS.read().unwrap()
        });
    }
}
