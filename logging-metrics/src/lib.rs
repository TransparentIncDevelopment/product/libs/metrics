//! This crate hooks up logging and metrics together to support emitting metrics based on logs

#![forbid(unsafe_code)]

use lazy_static::lazy_static;
use log::Record;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    PoisonError, RwLock, RwLockWriteGuard,
};
use tpfs_logger_port::EventObserver;
use xand_metrics::{DimensionedCtrDef, MetricOpts, Metrics, MetricsBag};

lazy_static! {
    static ref LOGGER_METRICS: RwLock<LoggingMetricsBag> = {
        RwLock::new(LoggingMetricsBag {
            error_count: DimensionedCtrDef::new(
                vec!["event"],
                MetricOpts::new(
                    "error_counter".to_string(),
                    "Count number of errors that have been logged".to_string(),
                ),
            ),
        })
    };
}

static IS_INITTED: AtomicBool = AtomicBool::new(false);

#[derive(MetricsBag)]
pub struct LoggingMetricsBag {
    error_count: DimensionedCtrDef,
}

impl LoggingMetricsBag {
    /// Initializes the global `LoggingMetricsBag` instance.
    ///
    /// # Errors
    /// If locking the global instance fails, an error is returned.
    pub fn init<MT: Metrics>(adapter: &MT) -> Result<(), PoisonError<RwLockWriteGuard<'_, Self>>> {
        let mut lm = LOGGER_METRICS.write()?;
        let _ = adapter.initialize(&mut *lm);
        IS_INITTED.store(true, Ordering::SeqCst);
        tpfs_logger_port::add_log_observer(MetricObserver);
        Ok(())
    }
}

struct MetricObserver;
impl EventObserver for MetricObserver {
    fn observe(&self, event_name: &'static str, _full_record: &Record<'_>) {
        if !IS_INITTED.load(Ordering::SeqCst) {
            // Silently do nothing if we aren't initted, which is an easy situation to be in in a lib
            return;
        }

        LOGGER_METRICS
            .read()
            .expect("Must be able to get lock on logger metrics")
            .error_count
            .with_dims(vec![event_name])
            .expect("Number of dimensions didn't line up. Impossible by construction.")
            .inc();
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use log::{Metadata, Record};
    use tpfs_logger_port::{error, logging_event};
    use xand_metrics_prometheus::PromMetrics;

    #[logging_event]
    #[derive(Serialize)]
    enum TestEvents {
        ThingOne,
    }

    struct NoOpLogger;
    impl log::Log for NoOpLogger {
        fn enabled(&self, _: &Metadata<'_>) -> bool {
            true
        }
        fn log(&self, _: &Record<'_>) {
            // I don't do anything!
        }
        fn flush(&self) {
            // I also do nothing!
        }
    }

    #[test]
    fn records_error_metrics() {
        log::set_boxed_logger(Box::new(NoOpLogger)).unwrap();
        let metrics = PromMetrics::default();
        LoggingMetricsBag::init(&metrics).unwrap();
        error!(TestEvents::ThingOne);
        // There should be one error recorded
        assert_eq!(
            LOGGER_METRICS
                .read()
                .unwrap()
                .error_count
                .with_dims(vec!["ThingOne"])
                .unwrap()
                .get(),
            1
        );
        error!(TestEvents::ThingOne);
        assert_eq!(
            LOGGER_METRICS
                .read()
                .unwrap()
                .error_count
                .with_dims(vec!["ThingOne"])
                .unwrap()
                .get(),
            2
        );
    }
}
