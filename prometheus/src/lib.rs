#![forbid(unsafe_code)]

mod error;

use crate::error::PromMetricErr;
use prometheus::{
    core::{
        AtomicF64, AtomicU64, GenericCounter, GenericCounterVec, GenericGauge, GenericGaugeVec,
    },
    Encoder, Histogram, HistogramOpts, HistogramVec, Opts, Registry, TextEncoder,
};
use xand_metrics::{
    Counter, Dimensioned, Gauge, MetricDefs, MetricError, Metrics, MetricsBag,
    MetricsSerializationContainer,
};

#[derive(Default)]
pub struct PromMetrics {
    registry: Registry,
}

impl Metrics for PromMetrics {
    type ErrorT = PromMetricErr;

    fn initialize<T: MetricsBag>(&self, metrics_bag: &mut T) -> Result<(), Self::ErrorT> {
        for md in metrics_bag.list_metrics() {
            match md {
                MetricDefs::Counter(ctr_def) => {
                    let real_ctr = GenericCounter::new(
                        ctr_def.opts().name.clone(),
                        ctr_def.opts().desc.clone(),
                    )?;
                    self.registry.register(Box::new(real_ctr.clone()))?;
                    ctr_def.populate(Box::new(PromCtr(real_ctr)));
                }
                MetricDefs::Gauge(g_def) => {
                    let real_gauge =
                        GenericGauge::new(g_def.opts().name.clone(), g_def.opts().desc.clone())?;
                    self.registry.register(Box::new(real_gauge.clone()))?;
                    g_def.populate(Box::new(PromGauge(real_gauge)))
                }
                MetricDefs::Histogram(h_def) => {
                    let opts =
                        HistogramOpts::new(h_def.opts().name.clone(), h_def.opts().desc.clone());
                    let real_histo = Histogram::with_opts(opts)?;
                    self.registry.register(Box::new(real_histo.clone()))?;
                    h_def.populate(Box::new(PromHistogram(real_histo)))
                }
                MetricDefs::DimensionedCtr(dim_def) => {
                    let opts = Opts::new(dim_def.opts().name.clone(), dim_def.opts().desc.clone());
                    let ctr_vec = GenericCounterVec::new(opts, dim_def.dimensions())?;
                    self.registry.register(Box::new(ctr_vec.clone()))?;
                    dim_def.populate(Box::new(PromVecCtr(ctr_vec)));
                }
                MetricDefs::DimensionedGauge(dim_def) => {
                    let opts = Opts::new(dim_def.opts().name.clone(), dim_def.opts().desc.clone());
                    let gauge_vec = GenericGaugeVec::new(opts, dim_def.dimensions())?;
                    self.registry.register(Box::new(gauge_vec.clone()))?;
                    dim_def.populate(Box::new(PromVecGauge(gauge_vec)));
                }
                MetricDefs::DimensionedHistogram(dim_def) => {
                    let opts = HistogramOpts::new(
                        dim_def.opts().name.clone(),
                        dim_def.opts().desc.clone(),
                    );
                    let histo_vec = HistogramVec::new(opts, dim_def.dimensions())?;
                    self.registry.register(Box::new(histo_vec.clone()))?;
                    dim_def.populate(Box::new(PromHistoVec(histo_vec)));
                }
            }
        }
        Ok(())
    }

    fn produce(&self) -> Result<MetricsSerializationContainer, Self::ErrorT> {
        // Gather the metrics.
        let mut buffer = vec![];
        let encoder = TextEncoder::new();
        let metric_families = self.registry.gather();

        encoder.encode(&metric_families, &mut buffer)?;

        let stringified = String::from_utf8(buffer)?;
        Ok(MetricsSerializationContainer {
            metrics: stringified,
        })
    }
}

struct PromCtr(GenericCounter<AtomicU64>);
impl Counter for PromCtr {
    fn inc(&self) {
        self.0.inc()
    }

    fn add(&self, delta: u64) {
        self.0.inc_by(delta)
    }

    fn get(&self) -> u64 {
        self.0.get()
    }
}
impl xand_metrics::Metric for PromCtr {}

struct PromGauge(GenericGauge<AtomicF64>);
impl Gauge for PromGauge {
    fn inc(&self) {
        self.0.inc()
    }

    fn add(&self, delta: f64) {
        self.0.add(delta)
    }

    fn get(&self) -> f64 {
        self.0.get()
    }

    fn set(&self, val: f64) {
        self.0.set(val)
    }

    fn dec(&self) {
        self.0.dec()
    }

    fn sub(&self, delta: f64) {
        self.0.sub(delta)
    }
}
impl xand_metrics::Metric for PromGauge {}

struct PromHistogram(Histogram);
impl xand_metrics::Histogram for PromHistogram {
    fn observe(&self, v: f64) {
        self.0.observe(v)
    }
}
impl xand_metrics::Metric for PromHistogram {}

struct PromVecCtr(GenericCounterVec<AtomicU64>);
impl xand_metrics::Metric for PromVecCtr {}
impl Dimensioned<dyn Counter> for PromVecCtr {
    fn with_dims(&self, dim_vals: Vec<&str>) -> Result<Box<dyn Counter>, MetricError> {
        Ok(Box::new(PromCtr(
            self.0
                .get_metric_with_label_values(&dim_vals)
                .map_err(|e| MetricError::IncorrectNumberOfDimensions {
                    msg: format!("{:?}", e),
                })?,
        )))
    }
}

struct PromVecGauge(GenericGaugeVec<AtomicF64>);
impl xand_metrics::Metric for PromVecGauge {}
impl Dimensioned<dyn Gauge> for PromVecGauge {
    fn with_dims(&self, dim_vals: Vec<&str>) -> Result<Box<dyn Gauge>, MetricError> {
        Ok(Box::new(PromGauge(
            self.0
                .get_metric_with_label_values(&dim_vals)
                .map_err(|e| MetricError::IncorrectNumberOfDimensions {
                    msg: format!("{:?}", e),
                })?,
        )))
    }
}

struct PromHistoVec(HistogramVec);
impl xand_metrics::Metric for PromHistoVec {}
impl Dimensioned<dyn xand_metrics::Histogram> for PromHistoVec {
    fn with_dims(
        &self,
        dim_vals: Vec<&str>,
    ) -> Result<Box<dyn xand_metrics::Histogram>, MetricError> {
        Ok(Box::new(PromHistogram(
            self.0
                .get_metric_with_label_values(&dim_vals)
                .map_err(|e| MetricError::IncorrectNumberOfDimensions {
                    msg: format!("{:?}", e),
                })?,
        )))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use xand_metrics::{
        CounterDef, DimensionedCtrDef, DimensionedGaugeDef, DimensionedHistoDef, GaugeDef,
        HistogramDef, MetricOpts,
    };

    #[derive(MetricsBag)]
    pub struct TestBag {
        pub test_ctr: CounterDef,
        pub test_gauge: GaugeDef,
        pub test_histo: HistogramDef,
        pub test_dim_ctr: DimensionedCtrDef,
        pub test_dim_gauge: DimensionedGaugeDef,
        pub test_dim_histo: DimensionedHistoDef,
    }

    impl Default for TestBag {
        fn default() -> Self {
            Self {
                test_ctr: CounterDef::new("test_counter", "ctr help"),
                test_gauge: GaugeDef::new("test_gauge", "gauge help"),
                test_histo: HistogramDef::new("test_histo", "histo help"),
                test_dim_ctr: DimensionedCtrDef::new(
                    vec!["a", "b", "c"],
                    MetricOpts::new(
                        "test_counter_vec".to_string(),
                        "I have dimensions!".to_string(),
                    ),
                ),
                test_dim_gauge: DimensionedGaugeDef::new(
                    vec!["a", "b", "c"],
                    MetricOpts::new(
                        "test_gauge_vec".to_string(),
                        "I have dimensions!".to_string(),
                    ),
                ),
                test_dim_histo: DimensionedHistoDef::new(
                    vec!["a", "b", "c"],
                    MetricOpts::new(
                        "test_histo_vec".to_string(),
                        "I have dimensions!".to_string(),
                    ),
                ),
            }
        }
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn counter() {
        let mut tb = TestBag::default();
        let prom_metrics = PromMetrics::default();
        let _ = prom_metrics.initialize(&mut tb);
        tb.test_ctr.inc();
        tb.test_ctr.inc();
        assert_eq!(tb.test_ctr.get(), 2);
        // Verify serialization is as expected
        let serialized = prom_metrics.produce().unwrap();
        assert_eq!(include_str!("../test_dat/counter"), serialized.metrics)
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn gauge() {
        let mut tb = TestBag::default();
        let prom_metrics = PromMetrics::default();
        prom_metrics.initialize(&mut tb).unwrap();
        tb.test_gauge.set(1337.420);
        // Verify serialization is as expected
        let serialized = prom_metrics.produce().unwrap();
        assert_eq!(include_str!("../test_dat/gauge"), serialized.metrics)
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn histo() {
        let mut tb = TestBag::default();
        let prom_metrics = PromMetrics::default();
        prom_metrics.initialize(&mut tb).unwrap();
        tb.test_histo.observe(10.0);
        tb.test_histo.observe(100.0);
        // Verify serialization is as expected
        let serialized = prom_metrics.produce().unwrap();
        assert_eq!(include_str!("../test_dat/histo"), serialized.metrics)
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn dim_counter() {
        let mut tb = TestBag::default();
        let prom_metrics = PromMetrics::default();
        prom_metrics.initialize(&mut tb).unwrap();
        tb.test_dim_ctr
            .with_dims(vec!["whatever!", "other", "dims"])
            .unwrap()
            .inc();
        // Verify serialization is as expected
        let serialized = prom_metrics.produce().unwrap();
        assert_eq!(include_str!("../test_dat/dim_counter"), serialized.metrics)
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn dim_gauge() {
        let mut tb = TestBag::default();
        let prom_metrics = PromMetrics::default();
        prom_metrics.initialize(&mut tb).unwrap();
        tb.test_dim_gauge
            .with_dims(vec!["one", "two", "three"])
            .unwrap()
            .set(11.22);
        // Verify serialization is as expected
        let serialized = prom_metrics.produce().unwrap();
        assert_eq!(include_str!("../test_dat/dim_gauge"), serialized.metrics)
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn dim_histo() {
        let mut tb = TestBag::default();
        let prom_metrics = PromMetrics::default();
        prom_metrics.initialize(&mut tb).unwrap();
        tb.test_dim_histo
            .with_dims(vec!["one", "two", "three"])
            .unwrap()
            .observe(10.0);
        tb.test_dim_histo
            .with_dims(vec!["one", "two", "three"])
            .unwrap()
            .observe(20.0);
        // Verify serialization is as expected
        let serialized = prom_metrics.produce().unwrap();
        assert_eq!(include_str!("../test_dat/dim_histo"), serialized.metrics)
    }
}
